package com.taotao.manage.mapper;

import com.github.abel533.mapper.Mapper;
import com.taotao.manage.pojo.ItemCat;

/**
 * Created by zsl on 2017/9/22.
 */
public interface ItemCatMapper extends Mapper<ItemCat> {
}
