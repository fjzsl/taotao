package com.taotao.manage.service;

import com.taotao.manage.mapper.ItemCatMapper;
import com.taotao.manage.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zsl on 2017/9/22.
 */
@Service
public class ItemCatService {
	@Autowired
	private ItemCatMapper itemCatMapper;

	public List<ItemCat> queryItemCatListByParentId(Long parentId) {
		ItemCat itemCat=new ItemCat();
		itemCat.setParentId(parentId);
		return this.itemCatMapper.select(itemCat);
	}
}
